﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class ExecutePlayable : MonoBehaviour
{
    [SerializeField]
    private PlayableDirector[] playables;
    private int nextPlayable = 0;

    public void StartNextPlayable()
    {
        playables[nextPlayable].Play();
       // Invoke("ReenablePlayerControl", (float)playables[nextPlayable].duration);
        nextPlayable++;
    }
    public void ReenablePlayerControl()
    {

    }
}