using UnityEngine;
public class HoleWorldHole : MonoBehaviour, IReachable
{
    public Viewpoint connectedViewpoint;
    public bool blocked = false;

    public GameObject glow;
    public SpriteRenderer hole;
    private bool canReach;
    public Color blockedColor;

    public AudioClip popInSound;
    public AudioClip popOutSound;
    AudioSource audio;
    Transform player;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        connectedViewpoint.connectedHole = this;
        player = GameObject.FindWithTag("Player").transform;
    }

    void Update()
    {
        if (blocked)
        {
            glow.SetActive(false);
            hole.color = blockedColor;
        }
        else
        {
            glow.SetActive(true);
            hole.color = Color.white;
        }
    }

    public void OnMouseUp()
    {
        if (!canReach)
            return;

        if (blocked)
            return;

        if (WorldController.Instance.transitioning)
            return;

        if (audio && popOutSound)
        {
            audio.clip = popOutSound;
            audio.Play();
        }

        player.gameObject.SetActive(false);

        WorldController.Instance.SetWorld(true, () =>
        {
            connectedViewpoint.EnterViewpoint();
        });
    }

    public void EnterHole()
    {
        WorldController.Instance.SetWorld(false, () =>
        {
            player.position = transform.position;
            player.gameObject.SetActive(true);
            if (audio && popInSound)
            {
                audio.clip = popInSound;
                audio.Play();
            }
        });
    }

    public bool IsReachable()
    {
        return canReach;
    }

    public void SetReachable(bool canReach)
    {
        this.canReach = canReach;
    }
}
