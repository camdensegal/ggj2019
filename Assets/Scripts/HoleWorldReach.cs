using UnityEngine;

public class HoleWorldReach : MonoBehaviour
{
    public float radius;
    public LayerMask mask;

    private Collider2D[] hits = new Collider2D[10];
    private int numHits = 0;
    void Update()
    {
        for (int i = 0; i < numHits; i++)
        {
            hits[i].GetComponentInParent<IReachable>().SetReachable(false);
        }
        numHits = Physics2D.OverlapCircleNonAlloc(transform.position, radius, hits, mask);
        for (int i = 0; i < numHits; i++)
        {
            hits[i].GetComponentInParent<IReachable>().SetReachable(true);
        }
    }
}
