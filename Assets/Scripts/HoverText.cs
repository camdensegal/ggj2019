﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HoverText : MonoBehaviour
{
    // Start is called before the first frame update

    public string itemEmotion;
    public Color color;
    private Image hoverBox;
    private Text itemText;

    void Start()
    {
        itemText = GameObject.FindWithTag("Canvas").transform.Find("UI/EmotionTextBox/EmotionText").GetComponent<Text>();
        hoverBox = GameObject.FindWithTag("Canvas").transform.Find("UI/EmotionTextBox").GetComponent<Image>();

        var particles = GetComponentInChildren<ParticleSystem>();
        var pMainSettings = particles.main;
        pMainSettings.startColor = color * pMainSettings.startColor.color;


        hoverBox.gameObject.SetActive(false);
    }

    void OnMouseOver()
    {
        hoverBox.gameObject.SetActive(true);
        itemText.text = itemEmotion;
    }

    void OnMouseExit()
    {
        // Don't hide if hover text overwritten elsewhere
        if (itemText.text != itemEmotion)
            return;

        hoverBox.gameObject.SetActive(false);
    }

    void OnDisable()
    {
        OnMouseExit();
    }

    void OnDestroy()
    {
        OnMouseExit();
    }
}
