public interface IReachable
{
    void SetReachable(bool canReach);
    bool IsReachable();
}
