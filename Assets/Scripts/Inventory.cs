﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public static Inventory inventory;

    [SerializeField]
    private PlaceableObject currentlyCarrying;
    [SerializeField]
    private Image uiObject;
    public List<RoomControl> rooms = new List<RoomControl>();

    public void Start()
    {
        if (inventory == null)
            inventory = this;
        else
            Destroy(gameObject);
    }
    public void CheckRooms()
    {
        foreach (RoomControl rc in rooms)
        {
            rc.CheckRoomState();
        }
    }

    public PlaceableObject GetCurrent()
    {
        return currentlyCarrying;
    }
    public void PickUp(PlaceableObject toPickUp)
    {
        if (toPickUp == null)
        {
            currentlyCarrying = null;
            uiObject.sprite = null;
            return;
        }

        currentlyCarrying = toPickUp;

        if (toPickUp.name != "Empty Spot")
        {
            uiObject.enabled = true;
            uiObject.sprite = toPickUp.GetComponentInChildren<SpriteRenderer>().sprite;
        }
        else
        {
            uiObject.sprite = null;
            uiObject.enabled = false;
        }
    }
}
