﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopingVariableSound : MonoBehaviour
{
    public AudioClip[] clips;
    public float frequency = 0.5f;

    private float lastPlayed;
    private AudioSource[] audioSources;
    private int curSource = 0;

    void Start()
    {
        audioSources = new AudioSource[5];
        for (int i = 0; i < 5; i++)
        {
            audioSources[i] = gameObject.AddComponent<AudioSource>();
            audioSources[i].loop = false;
            audioSources[i].playOnAwake = false;
        }
    }

    void Update()
    {
        if (Time.time - lastPlayed < frequency)
            return;

        audioSources[curSource].clip = clips[Random.Range(0, clips.Length)];
        audioSources[curSource].Play();

        curSource++;
        if (curSource >= audioSources.Length)
            curSource = 0;

        lastPlayed = Time.time;
    }
}
