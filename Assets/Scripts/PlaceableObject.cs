﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceableObject : MonoBehaviour
{
    public int objectID;
    private ParticleSystem particleSystem;

    void OnMouseUp()
    {
        var parent = GetComponentInParent<PlacementPosition>();
        if (parent)
            parent.OnMouseUp();
    }

    public void Shine(bool shouldShine)
    {
        if (!particleSystem)
            particleSystem = GetComponentInChildren<ParticleSystem>();

        if (shouldShine)
        {
            if (!particleSystem.isPlaying)
                particleSystem.Play();
        }
        else
        {
            if (particleSystem.isPlaying)
                particleSystem.Stop();
        }
    }
}
