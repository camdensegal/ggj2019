﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacementPosition : MonoBehaviour, IReachable
{
    public PlaceableObject occupiedBy = null;
    private bool canReach = false;
    private PlaceableObject objInstance = null;
    public string sortingLayer = "Default";
    public int sortingOrder = 0;
    private Collider2D col;

    public int objectID
    {
        get
        {
            if (occupiedBy == null)
                return -1;
            return occupiedBy.objectID;
        }
    }

    public bool Empty
    {
        get
        {
            return occupiedBy == null;

        }
    }

    void Start()
    {
        col = GetComponent<Collider2D>();
        UpdateView();
    }

    public void OnMouseUp()
    {
        if (!canReach)
            return;

        var curHeld = Inventory.inventory.GetCurrent();
        if (Empty && curHeld == null)
            return;

        var tObj = occupiedBy;
        occupiedBy = curHeld;
        Inventory.inventory.PickUp(tObj);
        Inventory.inventory.CheckRooms();
        UpdateView();
    }

    private void UpdateView()
    {
        if (objInstance != null)
            Destroy(objInstance.gameObject);

        if (!Empty)
        {
            objInstance = Instantiate<PlaceableObject>(occupiedBy, transform);
            objInstance.Shine(canReach);
            var rend = objInstance.GetComponentInChildren<SpriteRenderer>();
            rend.sortingLayerName = sortingLayer;
            rend.sortingOrder = sortingOrder;
            col.enabled = false;
        }
        else
        {
            col.enabled = true;
        }
    }

    public void SetReachable(bool canReach)
    {
        this.canReach = canReach;
        if (objInstance != null)
            objInstance.Shine(canReach);
    }

    public bool IsReachable()
    {
        return canReach;
    }
}
