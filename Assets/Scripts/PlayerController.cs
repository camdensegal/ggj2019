﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerController : PhysicsObject
{
    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;

    public AudioClip[] step;
    public AudioClip[] jump;
    public AudioClip[] land;

    public float stepFrequency = 0.3f;
    private float lastStep;

    private SpriteRenderer spriteRenderer;
    private Animator animator;

    private AudioSource[] audioSources;

    private bool wasGrounded = false;

    void Awake()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        animator = GetComponent<Animator>();

        audioSources = new AudioSource[5];
        for (int i = 0; i < 5; i++)
        {
            audioSources[i] = gameObject.AddComponent<AudioSource>();
            audioSources[i].loop = false;
            audioSources[i].playOnAwake = false;
        }
    }

    private int nextSource = 0;
    private void PlayClip(AudioClip clip)
    {
        PlayClip(new AudioClip[1] { clip });
    }
    private void PlayClip(AudioClip[] clips)
    {
        audioSources[nextSource].clip = clips[Random.Range(0, clips.Length)];
        audioSources[nextSource].Play();
        nextSource++;
        if (nextSource >= audioSources.Length)
            nextSource = 0;
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;
        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && grounded)
        {
            PlayClip(jump);
            velocity.y = jumpTakeOffSpeed;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }

        if (!wasGrounded && grounded)
        {
            PlayClip(land);
        }

        bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.05f) : (move.x < -0.05f));
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }

        animator.SetBool("grounded", grounded);
        animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);
        animator.SetFloat("velocityY", velocity.y);

        if (grounded && Mathf.Abs(velocity.x) > 0.1f && Time.time - lastStep > stepFrequency)
        {
            PlayClip(step);
            lastStep = Time.time;
        }

        targetVelocity = move * maxSpeed;
        wasGrounded = grounded;
    }
}
