﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* ITEM INDICES
 * Bread - 0
 * Wine - 1
 * Candle - 2
 * Family Picture - 3
 * Blanket - 4
 * House Plant - 5
 * Paint Brush - 6
 * Duffle Bag - 7
 * Vase - 8
 * Portrait - 9
 * Suitcase - 10
 * Book - 11
 * Winterhat - 12
 * Guitar - 13
 * Apron - 14
 */


public class RoomControl : MonoBehaviour
{
    public ExecutePlayable playable;
    public int requiredObjectID;
    public int secondRequiredObjectID;
    public bool readyForEvent;

    void Start()
    {
        Inventory.inventory.rooms.Add(this);
    }
    public void CheckRoomState()
    {
        if (!readyForEvent)
            return;
        bool hasFirstObject = false;
        bool hasSecondObject = false;
        foreach (Transform child in transform)
        {
            if (child.gameObject.GetComponent<Viewpoint>())
            {
                hasFirstObject = false;
                hasSecondObject = false;
                foreach (PlacementPosition pp in child.gameObject.GetComponent<Viewpoint>().reachableObjects)
                {
                    if (pp.objectID == requiredObjectID)
                    {
                        hasFirstObject = true;
                 //       Debug.Log("The First object is in the room");
                    }
                    if (pp.objectID == secondRequiredObjectID)
                    {
                        hasSecondObject = true;
                  //      Debug.Log("The Second object is in the room");
                    }
                    if (hasFirstObject && hasSecondObject)
                    {
                        readyForEvent = false;
                        playable.StartNextPlayable();
                        Debug.Log("The first object is in the room: " + hasFirstObject + "\nThe Second Object is in the room: " + hasSecondObject);
                        return;
                    }
                }
            }
        }
        Debug.Log("The first object is in the room: " + hasFirstObject + "\nThe Second Object is in the room: " + hasSecondObject);

    }
}
