﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpriteButton : MonoBehaviour
{
    public UnityEvent onClick;
    public bool exit = false;
    void OnMouseUp()
    {
        if (exit)
        {
            Application.Quit();
            return;
        }
        if (onClick != null)
            onClick.Invoke();
    }
}
