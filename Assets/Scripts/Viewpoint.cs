﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Viewpoint : MonoBehaviour
{
    public PlacementPosition[] reachableObjects;
    public bool activeViewpoint = false;
    [HideInInspector]
    public HoleWorldHole connectedHole;
    public AudioClip popInSound;
    public AudioClip popOutSound;

    private AudioSource audio;
    private ParticleSystem particles;

    public PlayableDirector playOnPeak;
    public bool prepPlayable = false;
    private SpriteRenderer renderer;
    private Animator animator;

    void Start()
    {
        if (activeViewpoint)
            EnterViewpoint();

        audio = GetComponent<AudioSource>();
        particles = GetComponentInChildren<ParticleSystem>();
        animator = GetComponent<Animator>();
    }
    public void EnterViewpoint()
    {
        activeViewpoint = true;
        foreach (var checkObject in reachableObjects)
            checkObject.SetReachable(true);

        if (prepPlayable && playOnPeak != null)
        {
            playOnPeak.Play();
            prepPlayable = false;
        }
        if (audio && popInSound)
        {
            audio.clip = popInSound;
            audio.Play();
        }
        if (particles)
            particles.Play();
        if (animator)
            animator.SetTrigger("popIn");
    }
    public void AbandonViewpoint()
    {
        if (WorldController.Instance.transitioning)
            return;

        foreach (var checkObject in reachableObjects)
            checkObject.SetReachable(false);

        activeViewpoint = false;
        connectedHole.EnterHole();

        if (audio && popOutSound)
        {
            audio.clip = popOutSound;
            audio.Play();
        }
        if (animator)
            animator.SetTrigger("popOut");
    }

    public void OnMouseUp()
    {
        if (activeViewpoint)
            AbandonViewpoint();
    }
}
