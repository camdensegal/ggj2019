using UnityEngine;
using UnityEngine.UI;
using System.Collections;
public class WorldController : MonoBehaviour
{
    public GameObject holeWorld;
    public GameObject homeWorld;
    public GameObject menu;
    public GameObject UI;

    public AudioSource holeMusic;
    public AudioSource homeMusic;

    public Image fader;

    public bool transitioning
    {
        get;
        private set;
    }

    public static WorldController Instance
    {
        get;
        private set;
    }

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        transitioning = false;
    }

    void Start()
    {
        fader.color = Color.clear;
        menu.SetActive(true);
        homeWorld.SetActive(true);
        homeMusic.volume = 1;
        holeMusic.volume = 0;
        holeWorld.SetActive(false);
        UI.SetActive(false);
    }

    public void StartPressed()
    {
        SetWorld(false);
    }

    public void Reset()
    {
        StopCoroutine("DoFade");
        Start();
    }

    public void SetWorld(bool isHouse, System.Action callback = null)
    {
        if (transitioning)
            return;

        if (homeWorld.activeSelf == isHouse && holeWorld.activeSelf != isHouse)
            return;

        transitioning = true;

        StartCoroutine(DoFade(isHouse, callback));
    }

    private IEnumerator DoFade(bool isHouse, System.Action callback)
    {
        while (fader.color.a < 1)
        {
            fader.color += new Color(0, 0, 0, Time.deltaTime * 2);
            float vol = fader.color.a / 2f;
            holeMusic.volume = isHouse ? 1 - vol : vol;
            homeMusic.volume = isHouse ? vol : 1 - vol;
            yield return null;
        }

        homeWorld.SetActive(isHouse);
        menu.SetActive(false);
        UI.SetActive(true);
        holeWorld.SetActive(!isHouse);

        while (fader.color.a > 0)
        {
            fader.color -= new Color(0, 0, 0, Time.deltaTime * 2);
            float vol = 1 - fader.color.a / 2f;
            holeMusic.volume = isHouse ? 1 - vol : vol;
            homeMusic.volume = isHouse ? vol : 1 - vol;
            yield return null;
        }

        transitioning = false;
        if (callback != null)
            callback();
    }
}
